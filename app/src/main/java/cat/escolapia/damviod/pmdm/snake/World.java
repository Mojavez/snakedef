package cat.escolapia.damviod.pmdm.snake;

import java.util.Random;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.05f;

    public Snake snake;
    public Diamond diamond;
    public Diamond diamond2;
    public Block Block;
    public boolean gameOver = false;;
    public int score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        snake = new Snake();
        placeDiamond();
        placeDiamond2();
        placeWall();
    }

    private void placeDiamond() {
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }

        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false) break;
            diamondX += 1;
            if (diamondX >= WORLD_WIDTH) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }
        }
        diamond = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
    }


    private void placeDiamond2() {
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }

        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false) break;
            diamondX += 1;
            if (diamondX >= WORLD_WIDTH) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }
        }
        diamond2 = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
    }

    private  void placeWall(){
        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }
        int blockX = random.nextInt(WORLD_WIDTH);
        int blockY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[blockX][blockY] == false) break;
            blockX += 1;
            if (blockX >= WORLD_WIDTH) {
                blockX = 0;
                blockY += 1;
                if (blockY >= WORLD_HEIGHT) {
                    blockY = 0;
                }
            }
        }
        Block = new Block(blockX, blockY);
    }

    public void update(float deltaTime) {
        if (gameOver) return;

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            if (snake.checkXoca()) {
                gameOver = true;
                return;
            }

            if (snake.checkDiamond(diamond)) {
                score = score + SCORE_INCREMENT;
                tick -= TICK_DECREMENT;
                snake.allarga();
                placeDiamond();
                return;
            }

            if (snake.checkDiamond2(diamond2)) {
                score = score + SCORE_INCREMENT;
                tick -= TICK_DECREMENT;
                snake.allarga();
                placeDiamond2();
                return;
            }

            if (snake.checkWall(Block)) {
                gameOver=true;
                return;
            }
            SnakePart head = snake.parts.get(0);
        }
    }
}

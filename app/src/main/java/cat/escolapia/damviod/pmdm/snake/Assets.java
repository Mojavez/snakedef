package cat.escolapia.damviod.pmdm.snake;

import cat.escolapia.damviod.pmdm.framework.Music;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

public class Assets {
    public static Pixmap background;
    public static Pixmap logo;
    public static Pixmap mainMenu;
    public static Pixmap buttons;
    public static Pixmap numbers;
    public static Pixmap ready;
    public static Pixmap pause;
    public static Pixmap gameOver;
    public static Pixmap headUp;
    public static Pixmap headLeft;
    public static Pixmap headDown;
    public static Pixmap headRight;
    public static Pixmap tail;
    public static Pixmap diamond;
    public static Pixmap kredits;
    public static Pixmap amor;
    public static Pixmap homer;
    public static Pixmap marge;
    public static Pixmap bart;
    public static Pixmap lisa;
    public static Pixmap maggie;
    public static Pixmap block;

    public static Sound click;
    public static Music music;
    public static Sound eat;
    public static Sound xoc;
}
